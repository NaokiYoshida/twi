class TweetsController < ApplicationController
  def index
      @tweet = Tweet.all
  end

  def new
      @tweet = Tweet.new
  end

  def create
      tweet = Tweet.new(message: params[:tweet][:message])
      tweet.save
      redirect_to tweets_path
  end

  def show
      @tweet = Tweet.find(params[:id])
  end

  def edit

  end
end
